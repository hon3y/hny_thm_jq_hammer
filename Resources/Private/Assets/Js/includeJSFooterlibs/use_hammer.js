/**
 * Created by hd on 07.01.16.
 */
var hive_thm_jq_hammer__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        if (typeof Hammer != 'undefined' && typeof Hammer == 'function') {

            clearInterval(hive_thm_jq_hammer__interval);

            if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                console.info('jQuery - Hammer loaded');
            }

        }

    }

}, 1000);